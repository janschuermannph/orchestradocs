Installation
============

.. toctree::
   :maxdepth: 2

   requirements
   frameworks



How to Install Orchestra
------------------------

Orchestra contains most of its own components in the source folder.
Therefore to install Orchestra, simply extract the contents of the
downloaded archive into an empty folder.


Install Composer Dependencies
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Orchestra uses a variety of external dependencies
such as Doctrines Database Abstraction Layer (DBAL)

To install these dependencies, simply run
:literal:`composer install` from your
project directory.

Read more about composer on the `Composer Project Page`_

.. _Composer Project Page: http://getcomposer.org


Server configuration
--------------------

Orchestra comes with a modified version of the
built-in PHP Development Server called :doc:`Opera <../cli/opera>`
A matching configuration file is also provided, for more
information see here: LINK

If you'd like to run Orchestra on a production server
or your own server [#compatible_server]_ you may need
to enable the :literal:`php_intl` [#php_intl]_ extension.

Orchestra requires at least PHP 5.5, older versions are :strong:`NOT`
supported!




.. [#compatible_server] :subscript:`Orchestra works best on Apache or Nginx on Ubuntu >12`
.. [#php_intl] :subscript:`Read more here: http://php.net/manual/en/book.intl.php`
